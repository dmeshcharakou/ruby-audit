# frozen_string_literal: true

require_relative 'lib/audit'
require_relative 'lib/audit/gem'

DEPS = Audit.dependencies

namespace :_ do
  desc 'Bootstrap data'
  task :bootstrap => :parse_gemfile_lock

  task :parse_gemfile_lock => 'data/gitlab_deps.json' do
  end

  rule 'data/gitlab_deps.json' do
    Audit.convert_deps
  end

  desc 'Cleanup data'
  task :cleanup do
    Dir['gems/*/*/repo'].each { |d| rm_rf d }
  end

  multitask :gem_info => [:parse_gemfile_lock] + DEPS.map(&:rubygems_path)
  multitask :gem_clone => [:gem_info] + DEPS.map(&:repo_path)
end

DEPS.each do |dep|
  desc "Audit gem #{dep.name}"
  task dep.name => [dep.version_path, dep.rubygems_path, dep.repo_path]

  rule dep.version_path do
    mkdir_p dep.version_path
  end

  rule dep.rubygems_path do |t|
    Rake::Task[dep.version_path].invoke

    Audit.download_rubygems(dep.name, dep.rubygems_path)
  end

  rule dep.repo_path do
    if dep.source_code_uri
      Audit.clone_repo(dep)
    else
      warn "WARN: #{dep.name} has no source code uri"
    end
  end
end
