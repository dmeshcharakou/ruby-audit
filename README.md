# Ruby 3 audit

See https://gitlab.com/gitlab-org/gitlab/-/issues/369392 and https://docs.google.com/spreadsheets/d/1gweXRsv0MdMXMyejuWpmGmp2T123geq2rlsJrdDh8r0

## What?

This repository helps to audit used gems at GitLab for Ruby 3 compatibility.

It does via:
- [x] Extract gem and version from `Gemfile.lock`
- [x] Lookup source url (if present) via RubyGems.org API per gem
- [x] Checkout source code with corresponding version
- [ ] Check if source code contains CI instructions to test for Ruby 3.0
- [ ] Pin dependencies of this gem which are used on production
- [ ] Run bundle and bundle exec rake (or spec or test) and check exit code
    - Or any other checks like running specific RuboCop rules

## How to contribute?

- Clone this repository
- Run `rake _:gem_clone` or `rake <gem name>` to check a specific gem
- Create a branch and commit the changes
- Submit an MR
