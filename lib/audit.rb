# frozen_string_literal: true

require 'json'
require 'net/http'
require 'open3'

require_relative 'audit/gem'

module Audit
  module_function

  extend FileUtils

  DEPS_LOCK = 'data/Gemfile.lock'
  DEPS_JSON = 'data/gitlab_deps.json'

  def dependencies
    return [] unless File.exist?(DEPS_JSON)

    JSON.parse(File.read(DEPS_JSON))
      .map { |name, version| Audit::Gem.new(name, version) }
  end

  def convert_deps
    File.read(DEPS_LOCK)
      .match(/GEM.*?specs:(.*)BUNDLED WITH/m)
      .captures[0]
      .then { _1.scan(/([\w_-]+)\s+\((\d+\.[^)]+)\)/).to_h }
      .then { JSON.pretty_generate(_1) }
      .tap { File.write(DEPS_JSON, _1) }
  end

  def sh_logged(logfile, *cmd)
    exit_code = nil

    File.open(logfile, 'a') do |log|
      Open3.popen3(*cmd) do |stdin, stdout, stderr, result|
        log.write(stdout.read)
        log.write(stderr.read)

        exit_code = result.value&.exitstatus || result.value.to_i
      end
    end

    exit_code == 0
  end

  def clone_repo(dep)
    logfile = dep.repo_logfile_path

    rm_rf dep.repo_path_tmp
    rm_rf dep.repo_path
    rm_rf logfile

    cloned = dep.each_git_clone do |*cmd|
      puts "Cloning #{cmd.join(' ')}"
      break true if sh_logged logfile, *cmd
    end

    if cloned
      puts "Cloning #{dep.name} SUCCEDED!"
      rm logfile
    else
      puts "Cloning #{dep.name} FAILED!"
    end

    mv dep.repo_path_tmp, dep.repo_path if File.directory?(dep.repo_path_tmp)
  end

  def download_rubygems(gem_name, info)
    puts "Downloading Rubygems info for #{gem_name}..."

    uri = URI("https://rubygems.org/api/v1/gems/#{gem_name}.json")
    info = JSON(Net::HTTP.get(uri)).select { |k, _| k.end_with?('_uri') || k == 'metadata' }

    File.write("gems/#{gem_name}/rubygems.json", JSON.pretty_generate(info))
  end
end
